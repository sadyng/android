package org.oursci.android.arborsci

import org.oursci.android.common.BaseActivity
import org.oursci.android.common.BaseNavigationManager

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 20.07.17.
 */
class NavigationManager(mainContainer: Int)
    : BaseNavigationManager(mainContainer) {
}