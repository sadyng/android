package org.oursci.android.bionutrientmeter.ui

import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.oursci.android.common.app
import org.oursci.android.common.server.WebServer
import org.oursci.android.common.ui.BaseFragment
import org.oursci.android.common.ui.IBackPressable
import org.oursci.android.bionutrientmeter.R
import org.oursci.android.bionutrientmeter.databinding.FragmentMainBinding

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 18.07.17.
 */
class MainFragment : BaseFragment(), IBackPressable {

    lateinit var binding: FragmentMainBinding
    lateinit var webserver: WebServer

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false)
        binding.btList.setOnClickListener {
            app().navigationManager.navigateToSurveyList()
            //activity.pickPlace()
            //app().navigationManager.navigateToPickLocation(-1)
        }

        binding.btBrowse.setOnClickListener {
            app().navigationManager.navigateToSurveyBrowser()
            //activity.pickPlace()
            //app().navigationManager.navigateToPickLocation(-1)
        }


        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity.allowOverview(false)
        activity.unlockMenus()

    }

    override fun onBackPressed(): Boolean {
        activity.finish()
        return true
    }

}