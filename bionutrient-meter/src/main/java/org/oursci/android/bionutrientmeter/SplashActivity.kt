package org.oursci.android.bionutrientmeter

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import org.oursci.android.bionutrientmeter.databinding.ActivitySplashBinding
import org.oursci.android.common.arch.ConsumableObserver
import org.oursci.android.common.ui.survey.browse.BrowseSurveysViewModel


/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 21.07.17.
 */
class SplashActivity : AppCompatActivity() {

    private val viewModel by lazy {
        ViewModelProviders.of(this, (applicationContext as App).viewModelFactory).get(BrowseSurveysViewModel::class.java)
    }

    lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash)

        viewModel.progress.observe(this, Observer {
            binding.progress.progress = (binding.progress.max * it).toInt()
        })

        viewModel.surveysLoadedResult.observe(this, ConsumableObserver { _, consumed ->
            val launchIntent = Intent(this, MainActivity::class.java)
            startActivity(launchIntent)
            finish()
        })

        viewModel.loadSurveys(downloadAll = true)
    }

}