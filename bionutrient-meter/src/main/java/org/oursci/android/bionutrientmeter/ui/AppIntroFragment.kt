package org.oursci.android.bionutrientmeter.ui

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import org.oursci.android.common.app
import org.oursci.android.common.arch.ConsumableObserver
import org.oursci.android.common.ui.intro.IntroFragment

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 17.07.17.
 */
class AppIntroFragment : IntroFragment() {

    override val viewModel: AppIntroViewModel by lazy {
        ViewModelProviders.of(this, app().viewModelFactory).get(AppIntroViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.loginLiveData.observe(this, ConsumableObserver {
            callback, consumed ->
            if(consumed){
                return@ConsumableObserver
            }

            callback?.let {
                activity.login {
                    success ->
                    it(success)
                }
            }
        })
    }

}