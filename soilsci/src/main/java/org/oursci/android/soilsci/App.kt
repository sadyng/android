package org.oursci.android.soilsci

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import org.oursci.android.common.BaseApplication
import org.oursci.android.common.BaseNavigationManager
import org.oursci.android.soilsci.ui.SoilIntroViewModel

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 18.07.17.
 */
class App : BaseApplication() {

    override val WEB_SERVER_PORT = 9095
    override val navigationManager: BaseNavigationManager = NavigationManager(R.id.content_main)
    override val TAG = "BioNutrientMeter"

    override val viewModelFactory = object : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(SoilIntroViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return SoilIntroViewModel(this@App) as T
            }

            return super@App.createViewModel(modelClass)
        }
    }


}


