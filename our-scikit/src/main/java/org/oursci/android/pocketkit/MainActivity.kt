package org.oursci.android.pocketkit

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.PersistableBundle
import android.preference.PreferenceManager
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import okhttp3.OkHttpClient
import okhttp3.Request
import org.oursci.android.common.BaseActivity
import org.oursci.android.common.ui.intro.IntroFragment
import org.oursci.android.common.ui.profile.ProfileFragment
import org.oursci.android.pocketkit.databinding.NavHeaderMainBinding
import timber.log.Timber
import java.io.IOException


class MainActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {


    companion object {
        val KEY_INTRO_DONE = "intro_done"
    }

    override val loginLogo: Int = R.drawable.ic_scikit_primary
    override val alertDialogTheme = R.style.AppThemeDialog
    override val loginTheme: Int? = R.style.FirebaseUIThemeBFA
    override val defaultSubtitle = BuildConfig.VERSION_NAME

    override val drawerLayout: DrawerLayout by lazy {
        drawer_layout
    }

    override val toolbar: Toolbar by lazy {
        findViewById<Toolbar>(R.id.toolbar)
    }

    override var openDrawerContentDesc = R.string.navigation_drawer_open
    override var closeDrawerContentDesc = R.string.navigation_drawer_close


    lateinit var navigationManager: NavigationManager
    lateinit var navHeaderBinding: NavHeaderMainBinding


    override val contentView = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        navigationManager = NavigationManager.from(app)
        navHeaderBinding = NavHeaderMainBinding.bind(nav_view.getHeaderView(0))


        navHeaderBinding.imageView.setOnClickListener {
            if (navigationManager.currentFragment() is ProfileFragment) {
                return@setOnClickListener
            }

            if (!app.user.loggedIn()) {
                login { success ->
                    Timber.d("login success: %s", success)

                    if (success) {
                        navHeaderBinding.user = app.user
                        navHeaderBinding.invalidateAll()
                    }
                }
                return@setOnClickListener
            }

            drawer_layout.closeDrawer(GravityCompat.START)
            navigationManager.profile()
        }

        fab_button.setOnClickListener { view ->
            view.visibility = if (view.visibility == View.VISIBLE) View.GONE else View.VISIBLE
            // navigationManager.deviceInfo()
        }



        if (savedInstanceState == null) {


            val introDone = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(KEY_INTRO_DONE, false)
            Timber.d("intro done is: %s", introDone)

            // add to back stack for sure
            navigationManager.intro(false)

            if (!introDone && navigationManager.currentFragment() !is IntroFragment) {
                Timber.d("reinstatiating intro fragment")
            } else {
                navigationManager.main()
            }
        }


        lockMenus()

        updateNavUser()

        nav_view.setNavigationItemSelectedListener(this)

        if (BuildConfig.DEBUG) {
            val client = OkHttpClient()
            val req = Request.Builder().url("http://app.nexus-computing.com/oursci/scikit.txt").build()
            client.newCall(req).enqueue(object : okhttp3.Callback {
                override fun onFailure(call: okhttp3.Call?, e: IOException?) {
                }

                override fun onResponse(call: okhttp3.Call?, response: okhttp3.Response?) {
                    if (response == null || call == null) {
                        return
                    }

                    if (!response.isSuccessful) {
                        return
                    }

                    try {
                        response.body()?.string()?.let {
                            if (it.trim().toInt() > BuildConfig.VERSION_CODE) {
                                showUpdateSnack()
                            }
                        }
                    } catch (e: Exception) {
                        Timber.e(e)
                    }
                }

            })
        }

    }

    private fun showUpdateSnack() {
        runOnUiThread {
            val snackBar = Snackbar.make(findViewById(R.id.coordinator),
                    "Updated App is available", Snackbar.LENGTH_INDEFINITE)

            snackBar.setAction("Download", { v ->
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse("http://app.nexus-computing.com/oursci/" +
                        "scikit.apk")
                startActivity(intent)
            })

            snackBar.show()
        }
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onSaveInstanceState(outState: Bundle?, outPersistentState: PersistableBundle?) {
        // no call to super
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        if (super.onOptionsItemSelected(item)) {
            return true
        }

        when (item?.itemId) {
            R.id.action_settings -> {
                navigationManager.settings()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        super.onNavigationItemSelected(item)
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.list_surveys -> {
                navigationManager.navigateToSurveyList()
            }
            R.id.list_survey_results -> {
                navigationManager.navigateToSurveyResultsList(false)
            }
            R.id.list_measurement_scripts -> {
                navigationManager.navigateToMeasurementScriptsList()
            }
            R.id.list_measurement_results -> {
                navigationManager.navigateToMeasurementResultsList()
            }
            R.id.browse -> {
                navigationManager.navigateToSurveyBrowser()
            }
            R.id.my_organizations -> {
                navigationManager.usersOrganizationList()
            }
            R.id.env_vars -> {
                navigationManager.navigateToEnvVars()
            }
        }

        FirebaseAuth.getInstance().currentUser?.getIdToken(false)



        FirebaseAuth.getInstance().addIdTokenListener { firebaseAuth: FirebaseAuth ->
            firebaseAuth.currentUser
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }


    override fun introDone() {
        supportActionBar?.show()
        unlockMenus()
        updateNavUser()

        PreferenceManager.getDefaultSharedPreferences(this)
                .edit().putBoolean(KEY_INTRO_DONE, true).apply()

        Handler().postDelayed({
            NavigationManager.from(app).main()
        }, 200)
    }


    fun updateNavUser() {
        navHeaderBinding.user = app.user
        navHeaderBinding.invalidateAll()
    }


    override fun onLoggedOut() {
        updateNavUser()
    }


    fun showIntro(): Unit {
        navigationManager.intro(true)
    }

    override fun isInIntro(): Boolean = true

}
