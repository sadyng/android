#!/bin/bash
FILE_NAME="`basename $(pwd)`.zip"
echo $FILE_NAME
cd out
cp ../manifest.json .
rm $FILE_NAME
zip -r $FILE_NAME processor.js sensor.js manifest.json
