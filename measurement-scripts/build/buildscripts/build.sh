#!/bin/bash
WEBPACK_BASE="`dirname $0`/../webpack"
NODE_PATH="`dirname $0`/../node_modules"

function usage(){
    echo "usage `basename $0` [target.js]"
}

BASENAME=`basename $0`
SCRIPT_DIR=`dirname $0`

if [ $BASENAME = "build-sensor.sh" ]; then
    echo "running build script for sensor"
    WEBPACK_CONFIG="$WEBPACK_BASE/sensor.config.js"
elif [ $BASENAME = "build-processor.sh" ]; then
    echo "running build script for processor"
    WEBPACK_CONFIG="$WEBPACK_BASE/processor.config.js"
elif [ $BASENAME = "build-processor-browser.sh" ]; then
    echo "running build script for processor for browser"
    WEBPACK_CONFIG="$WEBPACK_BASE/processor-browser.config.js"
else
    echo "run build-sensor.sh or build-processor.sh"
    exit 1
fi

if [ $# -ne 1 ]; then
    usage
    exit 1
fi


echo "using webpack config $WEBPACK_CONFIG"
echo $0
TARGET="`pwd`/$1"
echo $TARGET

$SCRIPT_DIR/../node_modules/.bin/webpack \
    --context $SCRIPT_DIR/.. \
    --entry $TARGET \
    --output-path "`dirname $TARGET`/out" \
    --output-filename "`basename $TARGET`" \
    --config $WEBPACK_CONFIG \
    --display verbose --display-origins --display-reasons --verbose



#./node_modules/.bin/webpack --config webpack/processor.config.js
#cd out
#zip -r processor.zip processor.js
