#include <DueTimer.h>

#define LED 13
#define PULSE_PIN 10
#define BUFFER_SIZE 16384

bool ledOn = false;
volatile uint16_t head = 0;
volatile boolean done = false;
volatile boolean active = false;
volatile uint16_t sweep_interval = 400;

uint16_t values[BUFFER_SIZE];

void ledIRQ()
{

    if (head >= BUFFER_SIZE)
    {
        return;
    }

    if (head % 4096 == 0)
    {
        sweep_interval *= 2;
        Timer4.start(sweep_interval);
    }

    ledOn = !ledOn;
    digitalWrite(LED, ledOn);
    values[head++] = analogRead(0);
}

void pulse()
{
    active = !active;
    digitalWrite(PULSE_PIN, active ? HIGH : LOW);
}

void setup()
{

    Serial.begin(115200);

    pinMode(LED, OUTPUT);
    pinMode(PULSE_PIN, OUTPUT);

    /*
    Timer3.attachInterrupt(ledIRQ);
    Timer4.attachInterrupt(pulse);
    */

    Serial.setTimeout(10000);
}

void loop()
{

    bool s = false;
    for (;;)
    {
        while (Serial.available())
        {
            char c = Serial.read();
            if (c == 'a')
            {
                s = true;
            }
        }

        if (s)
        {
            break;
        }
    }

    /*

    done = false;
    head = 0;
    active = true;

    Timer3.start(50); // micro seconds


    sweep_interval = 2000;
    Timer4.start(sweep_interval); // micro seconds
    loopMeasurement();
    */

    loopSimple();
}

void loopMeasurement()
{
    uint16_t i = 0;

    Serial.print("{\"data\":[");
    for (i = 0; i < BUFFER_SIZE; i++)
    {
        if (i != 0)
        {
            Serial.print(",");
        }
        Serial.print(values[i]);
    }
    Serial.print("]}\n");
}

void loopSimple()
{

    uint16_t len = 1024;
    uint16_t i = 0;

    digitalWrite(PULSE_PIN, HIGH);

    delay(1000);

    digitalWrite(PULSE_PIN, LOW);

    head = 0;
    for (i = 0; i < len; i++)
    {
        values[head++] = analogRead(0);
    }

    Serial.print("{\"data\":[");
    for (i = 0; i < len; i++)
    {
        if (i != 0)
        {
            Serial.print(",");
        }
        Serial.print(values[i]);
    }
    Serial.print("]}\n");
}