// run this file with nodejs for debugging
// $ node sensor.js

// export it with buildscripts/build/sensor.sh or build/project.sh
// and run it on your android device with
// curl -vvv [ANDROID_IP]:9095/upload_measurement --data-binary @out/measurement_script.zip
// make sure the web dev server in the android application is running (see android settings)


try {
    serialDevice = require("./config/port.json")
} catch (e) {
    console.log("please check the readme and create a ./conifg/port.json file with proper contents such as");
    serialDevice = {
        serialport: "/dev/ttyACM0"
    }
    console.log(JSON.stringify(serialDevice));
}
 
device = require("./lib/protocol-device.js");

app = require("../../build/hal/app.js");
serial = require("../../build/hal/serial.js");
reflectometer = require("../lib-reflectometer/reflectometer.js")

oboe = require('oboe');

run();

async function run() {


    // 2 Nov 2017
    // @greg
    // this is the new function call for accessing prior data
    // demoing this on "Carrot Sample Survey 1: Using frozen, shredded samples from freezer"
/*
    var m1 = app.getAnswer("measurement_1");
    if (m1 != null) {
        console.log("m1 available");
        try {
            var p = JSON.parse(m1);
            console.log("absorbance for of measurement_1 is: " + p.absorbance_4);
        } catch (e) {
            console.log("error: " + e)
        }
    } else {
        console.log("getting answer measurement_1 returned null");
    }
*/

    // uncomment for serial terminal mode (use ctrl c to exit and restart)
    /*
//    serial.write("configure_bluetooth+Reflectometer+115200+");
// object (flat or 3D.  Examples: leaves, apples)

// serial.write("set_user_defined+0+53453.16+1+53247.47+2+46890.24+3+35406.11+4+33729.22+5+27724.21+6+26690.21+7+46581.06+8+47273.26+9+46452.93+-1+");
// serial.write("set_user_defined+10+19236.78+11+19152.33+12+16934.18+13+12897.07+14+12309.65+15+9499.75+16+8567.92+17+9190.53+18+9577.57+19+9232.87+-1+");
// cuvette (liquid or solid.  Examples: dirt, water)
// serial.write("set_user_defined+20+56673.85+21+56541.86+22+56512.28+23+56857.52+24+59326.27+25+51086.92+26+58215.29+27+57896.67+28+57572.85+29+58064.09+-1+");
// serial.write("set_user_defined+30+26396.91+31+26340.26+32+26318.46+33+26401.77+34+27021.13+35+21599.73+36+22313.27+37+18850.65+38+19225.4+39+18971.43+-1++");
// droplet (Examples: plant extract for Brix)
// serial.write("set_user_defined+40+22916.29+41+22869.35+42+22858.46+43+22861.04+44+22999.74+45+21799.08+46+23714.94+47+38783.22+48+38143.6+49+39120.15+-1+");
// serial.write("set_user_defined+50+18116.52+51+18073.59+52+18060.88+53+18059.81+54+18123.1+55+15420.07+56+15196.68+57+15591.41+58+15784.68+59+15725.22+-1+");

serial.write("print_memory+");
 for(;;){
     try {
         var res = await device.readLine();
         console.log(res);
     } catch (err) {
         console.log("there was a problem: " + err);
     }
 }
 */

    /////////////////////////////////////////////////////////
    // Create protocols here
    // Standard protocols for object, cuvette, and droplet object types
    var reflectometer_object = reflectometer.pulse({
        object_type: "object",
        brightness: [550, 1200, 700, 250, 600, 1300, 1300, 45, 80, 100],
//        recall: ["userdef[0]", "userdef[1]", "userdef[2]", "userdef[3]", "userdef[4]", "userdef[5]", "userdef[6]", "userdef[7]", "userdef[8]", "userdef[9]", "userdef[10]", "userdef[11]", "userdef[12]", "userdef[13]", "userdef[14]", "userdef[15]", "userdef[16]", "userdef[17]", "userdef[18]", "userdef[19]"]

});
    var reflectometer_cuvette = reflectometer.pulse({
        object_type: "cuvette",
        brightness: [1500, 1500, 1300, 950, 900, 650, 550, 60, 60, 60],
        recall: ["userdef[20]", "userdef[21]", "userdef[22]", "userdef[23]", "userdef[24]", "userdef[25]", "userdef[26]", "userdef[27]", "userdef[28]", "userdef[29]", "userdef[30]", "userdef[31]", "userdef[32]", "userdef[33]", "userdef[34]", "userdef[35]", "userdef[36]", "userdef[37]", "userdef[38]", "userdef[39]"]
    });
    var reflectometer_droplet = reflectometer.pulse({
        object_type: "object",
        brightness: [700, 700, 700, 700, 700, 650, 550, 50, 50, 50],
        recall: ["userdef[40]", "userdef[41]", "userdef[42]", "userdef[43]", "userdef[44]", "userdef[45]", "userdef[46]", "userdef[47]", "userdef[48]", "userdef[49]", "userdef[50]", "userdef[51]", "userdef[52]", "userdef[53]", "userdef[54]", "userdef[55]", "userdef[56]", "userdef[57]", "userdef[58]", "userdef[59]"]
    });
    // Calibration protocols for each object type
    var reflectometer_object_shiney = reflectometer.pulse({
        brightness: [1500, 1500, 1300, 950, 900, 650, 550, 60, 60, 60],
        object_type: "object",
        calibration: "shiney"
    });
    var reflectometer_object_black = reflectometer.pulse({
        brightness: [1500, 1500, 1300, 950, 900, 650, 550, 60, 60, 60],
        object_type: "object",
        calibration: "black"
    });
    var reflectometer_cuvette_shiney = reflectometer.pulse({
        brightness: [1500, 1500, 1300, 950, 900, 650, 550, 60, 60, 60],
        object_type: "cuvette",
        calibration: "shiney"
    });
    var reflectometer_cuvette_black = reflectometer.pulse({
        brightness: [1500, 1500, 1300, 950, 900, 650, 550, 60, 60, 60],
        object_type: "cuvette",
        calibration: "black"
    });
    var reflectometer_droplet_shiney = reflectometer.pulse({
        brightness: [700, 700, 700, 700, 700, 650, 550, 50, 50, 50],
        object_type: "droplet",
        calibration: "shiney"
    });
    var reflectometer_droplet_black = reflectometer.pulse({
        brightness: [700, 700, 700, 700, 700, 650, 550, 50, 50, 50],
        object_type: "droplet",
        calibration: "black"
    });
    //    var testJSON = [{"calibration":1, "object_type": "object", "pulses":[60,60,60,60,60,60,60,60,60,60],"pulse_length":[[7],[7],[7],[7],[7],[7],[7],[7],[7],[7]],"pulse_distance":[1500,1500,1500,1500,1500,1500,1500,1500,1500,1500],"pulsed_lights":[[3],[4],[5],[9],[6],[7],[8],[1],[2],[10]],"pulsed_lights_brightness":[[700],[700],[700],[700],[700],[650],[600],[55],[55],[55]],"detectors":[[1],[1],[1],[1],[1],[1],[1],[0],[0],[0]],"dac_lights":1,"averages":5,"protocols":1,"recall":["userdef[0]","userdef[1]","userdef[2]","userdef[3]","userdef[4]","userdef[5]","userdef[6]","userdef[7]","userdef[8]","userdef[9]","userdef[10]","userdef[11]","userdef[12]","userdef[13]","userdef[14]","userdef[15]","userdef[16]","userdef[17]","userdef[18]","userdef[19]"]}]
    /////////////////////////////////////////////////////////

//--> FINAL!
//[{"pulses":[5,5,5,5,5,5,5,5,5,5],"pulse_length":[[7],[7],[7],[7],[7],[7],[7],[7],[7],[7]],"pulse_distance":[1500,1500,1500,1500,1500,1500,1500,1500,1500,1500],"pulsed_lights":[[3],[4],[5],[9],[6],[7],[8],[1],[2],[10]],"pulsed_lights_brightness":[[550],[1200],[700],[250],[600],[1300],[1300],[45],[80],[100]],"detectors":[[1],[1],[1],[1],[1],[1],[1],[0],[0],[0]],"dac_lights":1}] 


/////////////////////////////////////////////////////////
    // Check what type of device it is
    serial.write("device_info+\n");
    try {
        var info = await device.readJson();
        console.log(JSON.stringify(info, null, 2));

        if (info.device_name == "MultispeQ") {
            console.log("it's a multispeq")
            var p = reflectometer.pulse({
                pulses: 60,
                pulse_length: 7,
                pulse_distance: 1500,
                brightness: [200, 200, 200, 200, 200, 150, 150, 10, 10, 10],
                detectors: [0, 0, 0, 0, 0, 0, 0, 1, 1, 1]
            });
        } else {
            console.log("not a multispeq")
        }
    } catch (err) {
        console.log("there was a problem: " + err);
        app.result({})
    }

    await sleep(500);
    /////////////////////////////////////////////////////////



    /////////////////////////////////////////////////////////
    // Call protocol here
//    serial.write(JSON.stringify(reflectometer_object));
    serial.write(JSON.stringify([{"pulses":[60,60,60,60,60,60,60,60,60,60],"pulse_length":[[7],[7],[7],[7],[7],[7],[7],[7],[7],[7]],"pulse_distance":[1500,1500,1500,1500,1500,1500,1500,1500,1500,1500],"pulsed_lights":[[3],[4],[5],[9],[6],[7],[8],[1],[2],[10]],"pulsed_lights_brightness":[[550],[1200],[700],[250],[450],[1200],[1250],[40],[80],[100]],"detectors":[[1],[1],[1],[1],[1],[1],[1],[0],[0],[0]],"dac_lights":1, "averages":5, "object_type":"object","calibration":"no"}]));
//    values were determined based on the maximum value with a shiney object (white teflon backed by mylar) or the maximum value of the LED before LED is impacted by heat (lowest UV fits in that category - don't go above 650!)
//   light1+light2+light3+light4+light5+light6+light7+light8+light9+light10+
//   [{"environmental": [["temperature_humidity_pressure"]]}]
    /////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////
    // Receive data
    var count = 0;
    try {
        var res = await device.readJson("data_raw[*]", function (part) {
            count += 1;
            app.progress(0.1 + (count / 600.0 * 100.0 * .9));
        });

        app.progress(100);
        console.log("received serial data " + JSON.stringify(res));
        app.result(res);
    } catch (err) {
        console.log("there was a problem: " + err);
    }
    /////////////////////////////////////////////////////////

} //run


function sleep(duration) {
    return new Promise((resolve) => setTimeout(function () {
        resolve();
    }, duration));
}



