const { execSync } = require('child_process');
const path = require('path');
const fs = require('fs');
const readlineSync = require('readline-sync');
const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});
rl.stdoutMuted = true;
rl._writeToOutput = function _writeToOutput(stringToWrite) {
  if (rl.stdoutMuted) rl.output.write('*');
  else rl.output.write(stringToWrite);
};

/*
const pw = readlineSync.question('enter password for signer\n', {
  hideEchoBack: true,
});
*/

function main(pw) {
  const config = {
    projects: ['our-scikit', 'quick-carbon', 'bionutrient-meter'],
    sdkHome: path.join(process.env.HOME, '/android-sdk-linux'),
    oursciDir: path.join(process.env.HOME, '/aw/Oursci'),
    keystore: process.argv[2],
    outDir: process.argv[3],
  };

  if (!config.keystore || !fs.existsSync(config.keystore)) {
    console.log(`error, keystore does not exists: ${config.keystore}`);
    process.exit(1);
  }

  if (!config.outDir) {
    console.log('please specify out directory');
    process.exit(1);
  }

  if (!fs.existsSync(config.outDir)) {
    fs.mkdirSync(config.outDir);
  }

  if (!fs.existsSync(config.outDir)) {
    console.log(`error, outDir does not exists: ${config.outDir}`);
    process.exit(1);
  }

  process.chdir(config.oursciDir);

  const buildFile = fs.readFileSync('./build.gradle', 'utf-8');
  let version = null;
  buildFile.split('\n').forEach((line) => {
    if (line.indexOf('ext.build_tools_version') < 0) {
      return;
    }

    ({ 1: version } = line.split("'"));
  });

  if (!version) {
    console.log('error, build tools version version not defined');
    process.exit(1);
  }

  const tools = {
    zipalign: path.join(config.sdkHome, 'build-tools', version, 'zipalign'),
    apksigner: path.join(config.sdkHome, 'build-tools', version, 'apksigner'),
  };

  if (!fs.existsSync(tools.zipalign)) {
    console.log(`zipalign not found: ${tools.zipalign}`);
    process.exit(1);
  }

  if (!fs.existsSync(tools.apksigner)) {
    console.log(`apksigner not found: ${tools.apksigner}`);
    process.exit(1);
  }

  console.log(`using build tools version: ${version}`);

  config.projects.forEach((p) => {
    const signedApk = path.join(config.outDir, `${p}-signed.apk`);
    const alignedApk = path.join(config.outDir, `${p}-aligned.apk`);
    const sourceApk = path.join(
      config.oursciDir,
      p,
      'build',
      'outputs',
      'apk',
      'release',
      `${p}-release-unsigned.apk`,
    );

    console.log(`source apk: ${sourceApk}`);
    console.log(`aligned apk: ${alignedApk}`);
    console.log(`signed apk: ${signedApk}`);

    execSync(`./gradlew :${p}:assembleRelease`, { stdio: [0, 1, 2] });

    if (!fs.existsSync(sourceApk)) {
      console.log(`error, cannot find source apk: ${sourceApk}`);
      process.exit(1);
    }

    try {
      fs.unlinkSync(alignedApk);
    } catch (error) {
      console.log(error);
    }

    execSync(`${tools.zipalign} -v -p 4 ${sourceApk} ${alignedApk}`);

    if (!fs.existsSync(alignedApk)) {
      console.log(`error, cannot find aligned apk: ${alignedApk}`);
      process.exit(1);
    }
    try {
      fs.unlinkSync(signedApk);
    } catch (error) {
      console.log(error);
    }

    execSync(`${tools.apksigner} sign --ks ${config.keystore} --out ${signedApk} ${alignedApk}`, {
      input: pw,
    });

    if (!fs.existsSync(signedApk)) {
      console.log(`error, unable to sign apk: ${signedApk}`);
      process.exit(1);
    } else {
      console.log(`success building apk: ${signedApk}`);
    }
  });
}

rl.question('pw ', (pw) => {
  main(pw);
  rl.history = rl.history.slice(1);
  rl.close();
});
