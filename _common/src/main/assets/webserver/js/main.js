$(document).ready(function () {
    loadMonaco()
})

var editor;
function loadMonaco() {
    require.config({ paths: { 'vs': 'js/vs' } });
    require(['vs/editor/editor.main'], function () {
        editor = monaco.editor.create(document.getElementById('editor'), {
            value: [
                'function loop() {',
                '\tconsole.log("Hello world!");',
                '}'
            ].join('\n'),
            language: 'javascript'
        });


        monaco.languages.typescript.javascriptDefaults.addExtraLib([
            'declare class Serial {',
            '    /**',
            '     * Returns the a line from the Serial connection',
            '     */',
            '    static readLine():string',
            '    static write(data:string):int',
            '}',
        ].join('\n'), 'filename/serial.d.ts');
    });

    $("#run").click(runScript);
}

function runScript(){
    $.ajax({
        type : "POST",
        url : "./run",
        data : editor.getValue(),
        contentType : "text/plain"
    }).done(function(response){
        console.log(response);
        if(response.status == "error"){
            $("#editor-log").append("\n");
            $("#editor-log").append(atob(response.error));
            $("#editor-log").append("\n");
        } else {
            $("#editor-log").append("\nsuccess running script!\n");
        }
    });
}