package org.oursci.android.common.arch;

import androidx.annotation.Nullable;

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 27.07.17.
 */

public interface ConsumableObserver<T>{

    void onChanged(@Nullable T t, boolean consumed);
}
