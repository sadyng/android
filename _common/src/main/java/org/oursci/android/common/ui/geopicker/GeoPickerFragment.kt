package org.oursci.android.common.ui.geopicker


import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.model.LatLng
import org.oursci.android.common.app
import org.oursci.android.common.databinding.FragmentMapViewBinding
import org.oursci.android.common.ui.BaseFragment
import timber.log.Timber


/**
 * (c) Nexus-Computing GmbH Switzerland, 2019
 * Created by Manuel Di Cerbo on 26.07.19.
 *
 * THIS IS CURRENTLY NOT USED BY THE APP
 */
class GeoPickerFragment : BaseFragment() {

    lateinit var binding: FragmentMapViewBinding
    var mapView: MapView? = null
    var map: GoogleMap? = null


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    var selectedLocation = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentMapViewBinding.inflate(inflater, container, false)
        mapView = binding.mapView

        val mv = mapView ?: return binding.root

        mv.onCreate(savedInstanceState)
        mv.getMapAsync {
            map = it
            it.uiSettings.setAllGesturesEnabled(true)
            it.uiSettings.isCompassEnabled = true
            it.uiSettings.isMapToolbarEnabled = true
            it.uiSettings.isMyLocationButtonEnabled = true
            it.uiSettings.isZoomControlsEnabled = true
            it.isMyLocationEnabled = true
            it.mapType = GoogleMap.MAP_TYPE_HYBRID

            app().lastLocation { location ->
                val l = location ?: return@lastLocation
                it.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(l.latitude, l.longitude), 17f))
            }

            it.setOnCameraMoveListener {
                updateText(it)
            }
            updateText(it)
        }

        binding.tvCoordinates.setOnClickListener {

            val data = ClipData.newPlainText("Location", selectedLocation)
            val mgr = activity.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            mgr.primaryClip = data
            Toast.makeText(activity, "Copied to Clipboard", Toast.LENGTH_SHORT).show()
        }

        return binding.root
    }

    fun updateText(map: GoogleMap) {
        map.cameraPosition.target.let {
            it.latitude
            it.longitude
            selectedLocation = "${it.latitude}, ${it.longitude}"
            binding.tvCoordinates.text = "Selected Location (click to copy to clipboard)\n${it.latitude}, ${it.longitude}\n"
        }

        app().lastLocation { location ->
            map.cameraPosition.target.let {
                it.latitude
                it.longitude
                binding.tvCoordinates.text = "Selected Location (click to copy to clipboard)\n${it.latitude}, ${it.longitude}\nGPS Precision: ${location?.accuracy?.let { acc -> "$acc [m]" }
                        ?: "unknown"}"
            }
        }
    }


    var savedInstance: Bundle? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        savedInstance = savedInstanceState
        super.onCreate(savedInstanceState)
    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            binding.mapView.onDestroy()
        } catch (e: Exception) {
            Timber.d(e)
        }
    }

    override fun onPause() {
        try {
            binding.mapView.onPause()
        } catch (e: Exception) {
            Timber.e(e)
        }
        super.onPause()
    }

    override fun onResume() {
        try {
            binding.mapView.onResume()
        } catch (e: Exception) {
            Timber.d(e)

        }
        super.onResume()
    }

    override fun onStart() {
        try {
            binding.mapView.onStart()
        } catch (e: Exception) {
            Timber.d(e)

        }
        super.onStart()
    }

    override fun onStop() {
        try {
            binding.mapView.onStop()
        } catch (e: Exception) {
            Timber.e(e)
        }
        super.onStop()
    }

    override fun onLowMemory() {
        try {
            binding.mapView.onLowMemory()
        } catch (e: Exception) {
            Timber.d(e)
        }
        super.onLowMemory()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        try {
            binding.mapView.onSaveInstanceState(outState)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        super.onSaveInstanceState(outState)
    }

}