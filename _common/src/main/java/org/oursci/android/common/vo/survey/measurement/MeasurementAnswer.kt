package org.oursci.android.common.vo.survey.measurement

import org.joda.time.DateTime
import org.oursci.android.common.vo.survey.BaseAnswer

/**
 * Created by Manuel Di Cerbo on 29.09.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class MeasurementAnswer(
        override val answer: String? = null,
        answered: DateTime = DateTime(),
        modified: DateTime = DateTime(),
        anon: Boolean = false) : BaseAnswer<String>(answered, modified, anon) {
    override fun create(answer: String?, anon: Boolean) = if (this.answer == null) {
        MeasurementAnswer(answer, DateTime(), DateTime(), anon)
    } else {
        MeasurementAnswer(answer, this.answered, DateTime(), anon)
    }
}