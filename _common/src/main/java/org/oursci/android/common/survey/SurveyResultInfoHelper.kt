package org.oursci.android.common.survey

import org.joda.time.DateTime
import org.joda.time.Period
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.PeriodFormatterBuilder
import org.oursci.android.common.vo.survey.SurveyResultInfo
import timber.log.Timber
import java.io.File

/**
 * (c) Nexus-Computing GmbH Switzerland, 2018
 * Created by Manuel Di Cerbo on 02.05.18.
 */


fun SurveyResultInfo.created(): String {
    val created = try {
        timeFromName(instance)
    } catch (e: Throwable) {
        Timber.e(e)
        DateTime()
    }

    return formatter.print(Period(created, DateTime())).split(" ").take(2).joinToString(" ") {
        it.split("#").joinToString(" ")
    }.let { "%s ago".format(it) }
}




private fun timeFromName(file: File): DateTime {
    val time = file.name.substringAfter("instance_").substringBefore(".xml")
    Timber.d("time: %s", time)
    return DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").parseDateTime(time)
}

private val formatter = PeriodFormatterBuilder()
        .appendYears().appendSuffix("#years ")
        .appendMonths().appendSuffix("#months ")
        .appendDays().appendSuffix("#days ")
        .appendHours().appendSuffix("#h ")
        .appendMinutes().appendSuffix("#min ")
        .appendSeconds().appendSuffix("#s ")
        .printZeroNever()
        .toFormatter()