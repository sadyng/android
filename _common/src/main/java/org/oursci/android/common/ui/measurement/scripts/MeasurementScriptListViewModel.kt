package org.oursci.android.common.ui.measurement.scripts

import org.oursci.android.common.BaseApplication
import org.oursci.android.common.arch.ConsumableLiveData
import org.oursci.android.common.measurement.MeasurementLoader
import org.oursci.android.common.measurement.MeasurementScript
import org.oursci.android.common.ui.BaseViewModel
import timber.log.Timber
import kotlin.concurrent.thread

/**
 * Created by Manuel Di Cerbo on 12.10.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class MeasurementScriptListViewModel(app: BaseApplication) : BaseViewModel(app) {

    val availableScripsResult = ConsumableLiveData<Array<MeasurementScript>>()

    fun listMeasurementScripts(){
        Timber.d("listMeasurementScripts")

        thread {
            availableScripsResult.postValue(MeasurementLoader.of(app).getAllMeasurementScripts())
        }

    }
}