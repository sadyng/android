package org.oursci.android.common.ui.devices

import android.hardware.usb.UsbDevice
import org.oursci.android.common.btlib.BluetoothStatus
import org.json.JSONObject
import org.oursci.android.common.BaseApplication
import org.oursci.android.common.arch.ConsumableLiveData
import org.oursci.android.common.device.bt.BtManager
import org.oursci.android.common.device.usb.UManager
import org.oursci.android.common.ui.BaseViewModel


/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 10.10.17.
 */
class DeviceInfoViewModel(app: BaseApplication) : BaseViewModel(app) {

    val jsonDataResult = ConsumableLiveData<JSONObject>()

    fun reconnect() {
        connecting = true
        if (BtManager.of(app).hasDevice()) {
            BtManager.of(app).reconnect()
        } else if (UManager.of(app).hasDevice()) {
            UManager.of(app).apply {
                currentDevice()?.let {
                    if (!hasPermission(it)) {
                        requestPermission(it, { _: UsbDevice? ->
                            reconnect()
                            connectedResult.postValue(null)
                            connecting = false
                        })
                        return@apply
                    }
                }
                reconnect()
                connectedResult.postValue(null)
                connecting = false
            }
        }
    }

    val connectedResult = ConsumableLiveData<Void>()
    private var connecting = false

    var dataBuffer = StringBuffer(1000)
    val dataLock = Object()

    fun connecting() = connecting


    fun currentDeviceInfo() = if (BtManager.of(app).hasDevice()) {
        BtManager.of(app).currentDevice()
    } else {
        UManager.of(app).currentDevice()
    }

    fun isConnected(): Boolean = if (BtManager.of(app).hasDevice()) {
        BtManager.of(app).isConnected()
    } else {
        UManager.of(app).isConnected()
    }


    fun registerCallbacks() {
        BtManager.of(app).registerCallback(onScan = { _ ->

        }, onStatusChange = { status ->
            when (status) {
                BluetoothStatus.CONNECTED,
                BluetoothStatus.NONE -> {
                    connectedResult.postValue(null)
                    connecting = false
                }
                else -> {

                }
            }
        }, onDataRead = { data ->
            synchronized(dataLock) {
                dataBuffer.append(data)
                onNewData()
            }
        })

        UManager.of(app).registerCallback({ _ ->
        }, {}, { data ->
            synchronized(dataLock) {
                dataBuffer.append(data)
                onNewData()
            }
        })
    }

    private fun onNewData() {
        try {
            jsonDataResult.postValue(JSONObject(dataBuffer.toString()))
            dataBuffer.setLength(0) // clear buffer if successful
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun disconnect() {

    }

    fun fetchDeviceInfo() {
        sendData("device_info+")
    }

    private fun sendData(data: String) {
        if (BtManager.of(app).connected.get()) {
            BtManager.of(app).write(data)
        } else if (UManager.of(app).connected.get()) {
            UManager.of(app).write(data)
        }
    }
}