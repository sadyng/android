package org.oursci.android.common.ui.survey.geolocation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import org.oursci.android.common.app
import org.oursci.android.common.arch.ConsumableObserver
import org.oursci.android.common.databinding.ControlGeolocationResultBinding
import org.oursci.android.common.ui.BaseFragment
import org.oursci.android.common.ui.controls.geolocation.GeoLocationControl
import org.oursci.android.common.ui.controls.geolocation.GeoLocationDisplay
import timber.log.Timber

/**
 * Created by Manuel Di Cerbo on 03.11.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class GeoLocationResultFragment : BaseFragment() {

    val viewModel by lazyViewModel<GeoLocationViewModel>()


    private var idx: Int = -1
    private var lat: Double? = null
    private var lon: Double? = null


    lateinit var binding: ControlGeolocationResultBinding

    private var control: GeoLocationControl? = null


    private val controlObserver = ConsumableObserver<GeoLocationControl> { control, consumed ->
        if (consumed || control == null) {
            return@ConsumableObserver
        }

        this.control = control

        Timber.d("loaded control")

        binding.control = GeoLocationDisplay(control = control)
        binding.invalidateAll()

    }

    private val anonObserver = ConsumableObserver<Boolean> { success: Boolean?, consumed: Boolean ->
        if (consumed || success == null) {
            return@ConsumableObserver
        }

        (if (success) "Success updating" else "Failed to update").let { toast(it) }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, p2: Bundle?): View? {
        binding = ControlGeolocationResultBinding.inflate(inflater, container, false)
        activity.unlockMenus()


        idx = arguments?.getInt("idx", -1) ?: -1
        lat = arguments?.getDouble("lat")
        lon = arguments?.getDouble("lon")


        viewModel.updateAnon.observe(this, anonObserver)
        viewModel.loadControlLiveData.observe(this, controlObserver)
        viewModel.loadControl(idx, GeoLocationControl::class.java)


        if (idx == -1) {
            activity.allowOverview(false)
            binding.btNext.visibility = View.GONE
        } else {
            activity.allowOverview(true)
        }

        Timber.d("geo location, %f / %f", lat, lon)

        binding.progress.visibility = View.VISIBLE

        if (idx == -1) {
            binding.btNext.visibility = View.GONE
        }

        binding.btNext.setOnClickListener {
            if (viewModel.hasMoreQuestions(idx)) {
                app().navigationManager.moveToNextSurveyQuestion(idx + 1)
            } else {
                app().navigationManager.finalOverview()
            }
        }

        binding.btRedo.visibility = View.VISIBLE
        binding.btRedo.setOnClickListener {
            viewModel.redoLocation(idx)
        }

        if (idx == -1) {
            binding.btRedo.visibility = View.GONE
        }

        binding.btNext.visibility = View.VISIBLE


        binding.mapView.onCreate(savedInstance)

        binding.mapView.getMapAsync { googleMap ->
            //googleMap.isMyLocationEnabled = true
            googleMap.uiSettings.setAllGesturesEnabled(true)
            googleMap.mapType = GoogleMap.MAP_TYPE_HYBRID



            Timber.d("map is ready")
            binding.progress.visibility = View.GONE
            MapsInitializer.initialize(activity)


            lat?.let { latitude ->
                lon?.let { longitude ->
                    val pos = LatLng(latitude, longitude)
                    googleMap.addMarker(MarkerOptions()
                            .position(pos)
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                            .draggable(false)
                            .visible(true)
                    )

                    Timber.d("moving camera")
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pos, 15f))
                }
            }
        }

        return binding.root
    }

    var savedInstance: Bundle? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        savedInstance = savedInstanceState
        super.onCreate(savedInstanceState)
    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            binding.mapView.onDestroy()
        } catch (e: Exception) {
            Timber.d(e)
        }
    }

    override fun onPause() {
        try {
            binding.mapView.onPause()
        } catch (e: Exception) {
            Timber.e(e)
        }
        super.onPause()
    }

    override fun onResume() {
        binding.mapView.onResume()
        super.onResume()
    }

    override fun onStart() {
        binding.mapView.onStart()
        super.onStart()
    }

    override fun onStop() {
        try {
            binding.mapView.onStop()
        } catch (e: Exception) {
            Timber.e(e)
        }
        super.onStop()
    }

    override fun onLowMemory() {
        binding.mapView.onLowMemory()
        super.onLowMemory()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        try {
            binding.mapView.onSaveInstanceState(outState)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        super.onSaveInstanceState(outState)
    }


}