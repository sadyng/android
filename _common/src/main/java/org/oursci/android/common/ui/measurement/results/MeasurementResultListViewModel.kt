package org.oursci.android.common.ui.measurement.results

import org.oursci.android.common.BaseApplication
import org.oursci.android.common.arch.ConsumableLiveData
import org.oursci.android.common.measurement.MeasurementLoader
import org.oursci.android.common.measurement.MeasurementResultInfo
import org.oursci.android.common.ui.BaseViewModel
import kotlin.concurrent.thread

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 12.10.17.
 */
class MeasurementResultListViewModel(app: BaseApplication) : BaseViewModel(app) {


    val measurementsLoadedResult = ConsumableLiveData<Array<MeasurementResultInfo>>()

    fun loadMeasurementInfos() {
        thread {
            measurementsLoadedResult.postValue(MeasurementLoader.of(app).loadMeasurements())
        }
    }
}