package org.oursci.android.common.vo.survey.geolocation

import org.joda.time.DateTime
import org.oursci.android.common.vo.survey.BaseAnswer

/**
 * Created by Manuel Di Cerbo on 03.11.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class GeoLocationAnswer(
        override val answer: String? = null,
        answered: DateTime = DateTime(),
        modified: DateTime = DateTime(),
        anon: Boolean = false) : BaseAnswer<String>(answered, modified, anon) {
    override fun create(answer: String?, anon: Boolean): GeoLocationAnswer = if (this.answer == null) {
        GeoLocationAnswer(answer, DateTime(), DateTime(), anon)
    } else {
        GeoLocationAnswer(answer, this.answered, DateTime(), anon)
    }
}