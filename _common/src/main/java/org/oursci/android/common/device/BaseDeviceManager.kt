package org.oursci.android.common.device

import org.oursci.android.common.btlib.BluetoothStatus
import org.oursci.android.common.BaseApplication
import java.util.concurrent.atomic.AtomicBoolean

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 16.11.17.
 */
abstract class BaseDeviceManager<D>(val app: BaseApplication) {
    abstract val connected: AtomicBoolean
    abstract var currentDevice: D?
    abstract fun init()
    abstract fun registerCallback(onScan: (D) -> Unit,
                                  onStatusChange: (Any) -> Unit,
                                  onDataRead: (String) -> Unit,
                                  onScanDone: (() -> Unit)? = null)

    abstract fun scan()
    abstract fun connect(device: D)
    abstract fun reconnect()
    abstract fun write(data: String)
    abstract fun cancelConnect(deviceToConnect: D?)
    abstract fun currentDevice(): D?
    abstract fun isConnected(): Boolean
    abstract fun hasDevice(): Boolean
    fun getInterceptedData() = interceptBuffer.toString()

    private val interceptorMax = 5000000
    private var interceptBuffer = StringBuffer(1000)


    fun feedInterceptor(data: String) {
        synchronized(interceptBuffer) {
            interceptBuffer.append(data)

            val diff = interceptBuffer.length - interceptorMax

            if (diff > 0) {
                interceptBuffer.delete(0, diff)
            }
        }
    }
}