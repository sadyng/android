package org.oursci.android.common.ui.widget

import android.content.Context
import android.graphics.Rect
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.widget.AutoCompleteTextView
import android.widget.MultiAutoCompleteTextView

/*
Based on:
https://stackoverflow.com/questions/2126717/android-autocompletetextview-show-suggestions-when-no-text-entered/5783983#5783983
*/
class InstantAutoComplete : MultiAutoCompleteTextView {


    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet):   super(context, attrs)

    constructor(context: Context, attrs: AttributeSet?,    defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun enoughToFilter(): Boolean {
        return true
    }


    override fun onFocusChanged(focused: Boolean, direction: Int, previouslyFocusedRect: Rect?) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect)

        if(windowVisibility == View.GONE) {
            return
        }

        if(focused) {
            performFiltering(text, 0)
            showDropDown()
        }
    }




    override fun onTouchEvent(event: MotionEvent?): Boolean {

        if(event?.action == MotionEvent.ACTION_UP) {
            performFiltering(text, 0)
            showDropDown()
        }

        return super.onTouchEvent(event)

    }



}