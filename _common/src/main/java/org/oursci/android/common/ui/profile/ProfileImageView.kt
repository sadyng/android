package org.oursci.android.common.ui.profile

import android.content.Context
import android.util.AttributeSet
import android.widget.ImageView

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 17.08.17.
 */

class ProfileImageView : ImageView {


    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)


}