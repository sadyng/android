package org.oursci.android.common.ui.controls

import org.oursci.android.common.vo.survey.Group

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 05.09.17.
 */
abstract class BaseControl<T> {
    abstract var answer: T
    abstract val hint: String
    abstract val defaultValue: String
    abstract val label: String
    abstract val dataName: String
    abstract val readOnly: Boolean
    abstract val required: Boolean
    abstract val requiredText: String
    abstract val group: Group?

    open fun answer() : String? {
        return null
    }
}
