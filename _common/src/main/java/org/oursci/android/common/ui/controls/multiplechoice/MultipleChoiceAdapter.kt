package org.oursci.android.common.ui.controls.multiplechoice

import org.javarosa.core.model.data.SelectMultiData
import org.javarosa.core.model.data.SelectOneData
import org.javarosa.core.model.data.helper.Selection
import org.javarosa.form.api.FormEntryCaption
import org.javarosa.form.api.FormEntryPrompt
import org.oursci.android.common.BaseApplication
import org.oursci.android.common.survey.ControlType
import org.oursci.android.common.ui.controls.BaseControl
import org.oursci.android.common.ui.controls.IControlAdapter
import org.oursci.android.common.ui.controls.questionId
import org.oursci.android.common.ui.survey.mutliplechoice.MultipleChoiceFragment
import org.oursci.android.common.vo.survey.Group
import org.oursci.android.common.vo.survey.multiplechoice.MultipleChoiceAnswer
import timber.log.Timber

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 14.11.17.
 */
object MultipleChoiceAdapter : IControlAdapter<MultipleChoiceControl> {

    override fun produce(prompt: FormEntryPrompt, group: Group?): MultipleChoiceControl? {
        val options = mutableListOf<MultipleChoiceControl.Option>()
        prompt.selectChoices.forEach {
            Timber.d("a select choice: %s", it.value)

            val imgUrl = prompt.getSpecialFormSelectChoiceText(it, FormEntryCaption.TEXT_FORM_IMAGE)
            Timber.d("survey image id %s", imgUrl)

            options += MultipleChoiceControl.Option(it.value,
                    prompt.getSelectChoiceText(it) ?: "",
                    type = imgUrl?.let { MultipleChoiceControl.Option.Type.IMAGE }
                            ?: MultipleChoiceControl.Option.Type.TEXT,
                    url = imgUrl ?: ""
            )
        }

        val answerValue = prompt.answerValue

        return MultipleChoiceControl(
                answer = MultipleChoiceAnswer(
                        when (answerValue) {
                            is SelectOneData -> arrayOf((answerValue.value as Selection).value)
                            is SelectMultiData -> (answerValue.value as List<Selection>).map { it.value }.toTypedArray()
                            else -> null
                        }),
                options = ArrayList(options),
                multiSelect = ControlType.from(prompt.controlType) == ControlType.SELECT_MULTI,
                label = prompt.longText ?: "",
                readOnly = prompt.isReadOnly,
                required = prompt.isRequired,
                requiredText = "this input is required",
                hint = prompt.helpText ?: "",
                dataName = prompt.questionId(),
                group = group
        )
    }

    override fun resolve(app: BaseApplication, control: BaseControl<*>) = MultipleChoiceFragment()

}