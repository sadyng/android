package org.oursci.android.common.ui.controls.external

/**
 * Created by Manuel Di Cerbo on 17.03.19.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class ExternalControlDisplay(val control: ExternalControl, val url: String?, var error: String? = null) {


    fun textTitle() = control.label
    fun hint() = control.hint
    fun text() = control.answer() ?: ""
    fun textUrl() = url
    fun type() = when (control.params.type) {
        ExternalControl.Type.GENERIC -> ""
        ExternalControl.Type.FARMOS_AREA -> "FarmOS Area"
        ExternalControl.Type.FARMOS_PLANTING -> "FarmOS Planting"
        ExternalControl.Type.ONTOLOGY -> "Ontology"
    }

    fun textError() = error?.let{
        when (control.params.type) {
            ExternalControl.Type.GENERIC -> "No URL found"
            ExternalControl.Type.FARMOS_AREA -> "No FarmOS configuration available"
            ExternalControl.Type.FARMOS_PLANTING -> "No FarmOS configuration available"
            ExternalControl.Type.ONTOLOGY -> "No Ontology available"
        }
    }

}