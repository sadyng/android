package org.oursci.android.common.farmos

import androidx.room.Room
import org.oursci.android.common.BaseApplication


/**
 * Created by Manuel Di Cerbo on 15.03.19.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class FarmOsDb(app: BaseApplication) {
    val db by lazy {
        Room.databaseBuilder(app, FarmOsDatabase::class.java,
                "farmos")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build()
    }


    companion object {

        private val lock = Object()

        private var sInstance: FarmOsDb? = null

        fun of(app: BaseApplication): FarmOsDb {
            synchronized(lock) {
                if (sInstance == null) {
                    sInstance = FarmOsDb(app)
                }

                return sInstance!!
            }
        }
    }
}