package org.oursci.android.common.ui.survey.overview

import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject
import org.oursci.android.common.BaseApplication
import org.oursci.android.common.answered
import org.oursci.android.common.arch.ConsumableLiveData
import org.oursci.android.common.status
import org.oursci.android.common.survey.Palette
import org.oursci.android.common.survey.SurveyManager
import org.oursci.android.common.ui.BaseViewModel
import org.oursci.android.common.ui.controls.BaseControl
import org.oursci.android.common.ui.controls.external.ExternalControl
import org.oursci.android.common.ui.controls.geolocation.GeoLocationControl
import org.oursci.android.common.ui.controls.measurement.MeasurementControl
import org.oursci.android.common.ui.controls.multiplechoice.MultipleChoiceControl
import org.oursci.android.common.ui.controls.text.TextControl
import org.oursci.android.common.ui.controls.text.TextControlValidator
import org.oursci.android.common.ui.survey.geolocation.GeoLocationFragment
import org.oursci.android.common.vo.survey.BaseAnswer
import org.oursci.android.common.vo.survey.Group
import org.oursci.android.common.vo.survey.Survey
import org.oursci.android.common.vo.survey.SurveyOverviewItem
import timber.log.Timber
import java.io.File
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.concurrent.thread

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 18.09.17.
 */
class SurveyOverviewViewModel(app: BaseApplication) : BaseViewModel(app) {

    private val surveyManager = SurveyManager.of(app)
    val loadSurveyControlsLiveData = ConsumableLiveData<Array<SurveyOverviewItem>>()
    val exportDataResult = ConsumableLiveData<String>()
    val uploadResult = ConsumableLiveData<Survey>()

    val exporting = AtomicBoolean(false)

    class Overview(
            val answer: String,
            val done: Boolean,
            val anon: Boolean,
            val warning: Boolean = false,
            val error: Boolean = false)

    fun loadControls() {
        Timber.d("Survey Summary Script: %s", surveyManager.currentSurvey?.summaryScript)
        val survey = surveyManager.currentSurvey
                ?: Timber.e("no current survey found").let { return }
        var currentGroup: Group? = null

        val res = survey.controls.mapIndexed { index, baseControl: BaseControl<*> ->

            val relevant = survey.indexMap.entries.find { (control, index) ->
                control.dataName == baseControl.dataName
            }?.value?.let {
                survey.fec.model.isIndexRelevant(it)
            } ?: false

            val firstInGroup = when {
                baseControl.group == null -> false
                baseControl.group !== currentGroup -> true
                else -> false
            }

            val nextGroup = survey.controls.getOrNull(index + 1)?.group
            val lastInGroup = nextGroup !== baseControl.group

            currentGroup = baseControl.group


            val overview = when (baseControl) {
                is TextControl -> answer(baseControl)
                is ExternalControl -> answer(baseControl)
                is MultipleChoiceControl -> answer(baseControl)
                is MeasurementControl -> answer(baseControl)
                is GeoLocationControl -> answer(baseControl)
                else -> Overview("", done = false, anon = false)
            }

            SurveyOverviewItem("${index + 1}", baseControl.label,
                    overview.answer, overview.done, overview.anon,
                    relevant = relevant,
                    group = baseControl.group,
                    firstInGroup = firstInGroup,
                    lastInGroup = lastInGroup,
                    warning = overview.warning,
                    error = overview.error)
        }
        loadSurveyControlsLiveData.postValue(res?.toTypedArray() ?: emptyArray())
    }

    private fun answer(externalControl: ExternalControl): Overview {
        val done = externalControl.answer()?.isNotEmpty() ?: false

        val current = externalControl.answer.answer?.let {
            if (!it.isEmpty()) "$it<br>" else ""
        } ?: ""

        val anon = externalControl.answer.anon

        if (externalControl.required && current.isBlank()) {
            return Overview("$current<i><font color='red'>Answer required</font></i>", done, anon)
        }

        externalControl.answer.answer?.let {
            return Overview(it, done, anon)
        }

        return Overview("", done, anon)
    }

    private fun answer(geoLocationControl: GeoLocationControl): Overview {

        val error = errorIfEmptyButRequired(geoLocationControl)
        val done = geoLocationControl.answer()?.isNotEmpty() ?: false


        geoLocationControl.let {
            return Overview(error ?: it.answer() ?: "", done, geoLocationControl.answer.anon)
        }
    }

    private fun answer(multipleChoiceControl: MultipleChoiceControl): Overview {

        val error = errorIfEmptyButRequired(multipleChoiceControl)
        val done = multipleChoiceControl.answer()?.isNotEmpty() ?: false

        return Overview(error
                ?: multipleChoiceControl.answer() ?: "", done, multipleChoiceControl.answer.anon)
    }


    private fun answer(measurementControl: MeasurementControl): Overview {
        val error = measurementControl.status() == MeasurementControl.Companion.Status.ERROR
        val warning = measurementControl.status() == MeasurementControl.Companion.Status.WARNING
        val done = when (measurementControl.status()) {
            MeasurementControl.Companion.Status.SUCCESS,
            MeasurementControl.Companion.Status.ERROR,
            MeasurementControl.Companion.Status.WARNING -> true
            else -> false
        }

        Timber.d("answer error is $error")
        Timber.d("answer done is $done")


        if (!done) {
            Timber.d("answer return from !done")
            return Overview(errorIfEmptyButRequired(measurementControl)
                    ?: "", done, measurementControl.answer.anon)
        }


        measurementControl.answer.answer?.let { json ->
            try {
                JSONObject(json).getJSONObject("data").let { data ->
                    var idx = 0
                    val buffer = StringBuffer()
                    data.keys().forEach {
                        buffer.append("<font color='#%06X'>%s</font> ".format(
                                Palette.getColor(idx++),
                                data.getString(it)
                        ))
                    }

                    Timber.d("answer return from try")
                    return Overview(buffer.toString().let {
                        if (it.length > 200) {
                            it.substring(0, 200)
                        } else {
                            it
                        }
                    }, done, measurementControl.answer.anon, warning = warning, error = error)
                }
            } catch (e: Exception) {
                Timber.d("answer return from catch")
                return Overview((if (json.length > 200) json.substring(0, 200) else json), done, measurementControl.answer.anon, warning = warning, error = error)
            }
        }

        Timber.d("answer return from end")

        return Overview(errorIfEmptyButRequired(measurementControl)
                ?: "", done, measurementControl.answer.anon, warning = warning, error = error)
    }

    private fun answer(textControl: TextControl): Overview {
        val model = TextControlValidator(textControl)
        val done = textControl.answer()?.isNotEmpty() ?: false

        val error = model.errorMessage()
        val current = textControl.answer.answer?.let {
            if (!it.isEmpty()) "$it<br>" else ""
        } ?: ""

        val anon = textControl.answer.anon

        if (!error.isEmpty()) {
            return Overview("$current<i><font color='red'>$error</font></i>", done, anon)
        }

        textControl.answer.answer?.let {
            return Overview(it, done, anon)
        }

        return Overview("", done, anon)
    }

    fun deleteSurvey(): Boolean {
        try {
            thread {
                // should do a callback
                SurveyManager.of(app).deleteCurrentSurveyInstance()
            }
            return true
        } catch (e: Exception) {
            Timber.e(e)
        }

        return false
    }

    fun export() {
        if (exporting.getAndSet(true)) {
            return
        }

        thread {
            val path = try {
                SurveyManager.of(app).exportCurrentSurvey()
            } catch (e: Exception) {
                e.printStackTrace()
                null
            }

            exportDataResult.postValue(path)
            exporting.set(false)
        }

    }

    fun uploadSurvey() {
        if (exporting.getAndSet(true)) {
            return
        }

        thread {
            val mgr = SurveyManager.of(app)
            try {
                mgr.currentSurvey?.let(mgr::uploadSurvey)
                uploadResult.postValue(mgr.currentSurvey)
            } catch (e: Exception) {
                uploadResult.postValue(null)
                Timber.e(e)
            }
            exporting.set(false)
        }

    }

    private fun errorIfEmptyButRequired(control: BaseControl<*>): String? {
        if (!control.required) {
            return null
        }


        fun requiredText(): String {
            if (control.requiredText.isNotBlank()) {
                return "<i><font color='red'>${control.requiredText}</font></i>"
            }
            return "<i><font color='red'>Answer Required</font></i>"
        }


        when (control) {
            is MeasurementControl -> {
                if (!control.answered()) {
                    return requiredText()
                }
                return null
            }
            else -> {
                val a = control.answer()
                if (a == null || a.isBlank()) {
                    return requiredText()
                }
                return null
            }
        }

    }


}