package org.oursci.android.common.ui.envvar

import android.content.DialogInterface
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import org.joda.time.DateTime
import org.oursci.android.common.BaseActivity
import org.oursci.android.common.R
import org.oursci.android.common.app
import org.oursci.android.common.arch.ConsumableObserver
import org.oursci.android.common.databinding.DialogEnvvarBinding
import org.oursci.android.common.databinding.FragmentGenericListBinding
import org.oursci.android.common.envvars.EnvVar
import org.oursci.android.common.envvars.EnvVarDb
import org.oursci.android.common.ui.BaseFragment
import org.oursci.android.common.ui.GenericItemAdapter
import org.oursci.android.common.ui.GenericItemDisplay
import kotlin.concurrent.thread

/**
 * (c) Nexus-Computing GmbH Switzerland, 2018
 * Created by Manuel Di Cerbo on 14.05.18.
 */
class EnvVarListFragment : BaseFragment() {

    private lateinit var binding: FragmentGenericListBinding

    private val viewModel by lazyViewModel<EnvVarViewModel>()

    private val adapter: GenericItemAdapter<EnvVar> = GenericItemAdapter({ envVar ->
        GenericItemDisplay(title = envVar.key, description = "${envVar.description}<br>${envVar.value}")
    },
            onClick = { envVar: EnvVar, _: View ->
                showVarDialog(envVar)
            }
    )

    private val resultObserver = ConsumableObserver<List<EnvVar>>({ envVars: List<EnvVar>?, consumed: Boolean ->
        if (consumed || envVars == null) {
            return@ConsumableObserver
        }


        adapter.items = envVars
        adapter.notifyDataSetChanged()


        binding.recyclerview.visibility = View.VISIBLE
        binding.progress.visibility = View.GONE

        if (envVars.isEmpty()) {
            binding.tvHeader.text = "No Variables set"
            binding.btAction.text = "Create"

            binding.tvHeader.visibility = View.VISIBLE
            binding.btAction.visibility = View.VISIBLE
        } else {
            binding.tvHeader.visibility = View.GONE
            binding.btAction.visibility = View.GONE
        }
    })

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentGenericListBinding.inflate(inflater, container, false)
        binding.recyclerview.adapter = adapter

        binding.recyclerview.adapter = adapter
        binding.recyclerview.layoutManager = LinearLayoutManager(activity)
        binding.recyclerview.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))


        if (viewModel.busy.get()) {
            binding.recyclerview.visibility = View.GONE
            binding.progress.visibility = View.VISIBLE
        }

        binding.btAction.setOnClickListener {
            showVarDialog(null)
        }

        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.envVarResult.observe(this, resultObserver)
        viewModel.loadEnvVars()
        super.onViewCreated(view, savedInstanceState)
    }

    override fun menuItems(): Array<BaseActivity.MenuItemInfo> = arrayOf(
            BaseActivity.MenuItemInfo(Menu.NONE, R.id.menu_add, 0, "Add Variable", R.drawable.ic_add_circle_black_24dp, true)
    )

    override fun onMenuItemPressed(menuItemId: Int): Boolean = when (menuItemId) {
        R.id.menu_add -> {
            showVarDialog(null)
            true
        }
        else -> super.onMenuItemPressed(menuItemId)
    }

    private fun showVarDialog(envVar: EnvVar?) {

        val contentView = DialogEnvvarBinding.inflate(LayoutInflater.from(activity), null)

        if (envVar != null) {
            contentView.etName.isEnabled = false
            contentView.etName.setText(envVar.key)

            contentView.etValue.setText(envVar.value)
            contentView.etDesc.setText(envVar.description)
        }


        val dialog = AlertDialog.Builder(activity, activity.alertDialogTheme)
                .setTitle(envVar?.let { "Edit: ${envVar.key}" } ?: "Create new variable")
                .setView(contentView.root)
                .setPositiveButton("Save", { dialog: DialogInterface?, which: Int ->
                    dialog ?: return@setPositiveButton
                    validateInput(contentView)?.let {
                        thread {
                            EnvVarDb.of(app()).db.envVarDao().insert(it)
                            viewModel.loadEnvVars()
                        }
                        dialog.dismiss()
                    } ?: contentView.invalidateAll()
                }).apply {
                    envVar?.let {
                        setNegativeButton("Delete", { dialog: DialogInterface?, _: Int ->
                            dialog ?: return@setNegativeButton

                            thread {
                                EnvVarDb.of(app()).db.envVarDao().deleteEnvVar(it)
                                viewModel.loadEnvVars()
                            }
                            dialog.dismiss()
                        })
                    }
                }

                .create()
        dialog.show()
    }

    private fun validateInput(binding: DialogEnvvarBinding): EnvVar? {
        binding.etName.error = ""
        binding.etValue.error = ""

        if (binding.etName.text?.isBlank() != false) {
            binding.etName.error = "Name may not be empty"
            return null
        }

        /*
        if (binding.etValue.text.isBlank()) {
            binding.etValue.error = "Value may not be empty"
            return null
        }
        */

        return EnvVar(
                key = binding.etName.text.toString(),
                value = binding.etValue.text.toString(),
                description = binding.etDesc.text.toString(),
                modified = DateTime.now().millis
        )
    }

}