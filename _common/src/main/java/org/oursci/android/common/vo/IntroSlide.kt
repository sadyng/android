package org.oursci.android.common.vo

import android.graphics.drawable.Drawable
import android.view.View

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 17.07.17.
 */

data class IntroSlide(val imageDrawable: Int, val title: String, val content: String,
                      val buttonText: String = "", val buttonPressed: (() -> Unit)? = null) {

    fun buttonVisibility() = if (buttonText.isEmpty()) View.GONE else View.VISIBLE
}