package org.oursci.android.common.ui.devices

import android.text.Spanned
import org.oursci.android.common.btlib.BluetoothStatus
import org.json.JSONObject
import org.oursci.android.common.BaseApplication
import org.oursci.android.common.arch.ConsumableLiveData
import org.oursci.android.common.device.bt.BtManager
import org.oursci.android.common.device.usb.UManager
import org.oursci.android.common.html
import org.oursci.android.common.ui.BaseViewModel
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.concurrent.thread

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 17.10.17.
 */
class DeviceDebugViewModel(app: BaseApplication) : BaseViewModel(app) {

    val refreshResult = ConsumableLiveData<Void>()
    private val refreshing = AtomicBoolean(false)

    var data: Spanned = html("")

    fun refreshLog() {
        if (refreshing.get()) {
            return
        }

        refreshing.set(true)
        thread {
            data = html(
                    if (BtManager.of(app).hasDevice()) {
                        BtManager.of(app).getInterceptedData()
                    } else {
                        UManager.of(app).getInterceptedData()
                    }
            )
            refreshResult.postValue(null)
            refreshing.set(false)
        }
    }

    var dataBuffer = StringBuffer(1000)
    val dataLock = Object()


    fun isConnected(): Boolean = if (BtManager.of(app).hasDevice()) {
        BtManager.of(app).isConnected()
    } else {
        UManager.of(app).isConnected()
    }


    fun registerCallbacks() {
        BtManager.of(app).registerCallback(
                onScan = {},
                onStatusChange = {},
                onDataRead = { data ->
                    synchronized(dataLock) {
                        dataBuffer.append(data)
                        onNewData()
                    }
                })
        UManager.of(app).registerCallback({}, {}, { data ->
            synchronized(dataLock) {
                dataBuffer.append(data)
                onNewData()
            }
        })
    }

    private fun onNewData() {
        try {
            refreshLog()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun sendData(data: String) {
        if (BtManager.of(app).connected.get()) {
            BtManager.of(app).write(data)
        } else if (UManager.of(app).connected.get()) {
            UManager.of(app).write(data)
        }
    }

}