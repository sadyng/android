package org.oursci.android.common.ui.controls

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 05.09.17.
 */
abstract class BaseControlModel {

    open fun errorMessage(): String = ""
    open fun valid(): Boolean = true
}