package org.oursci.android.common.resources

import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONArray
import org.oursci.android.common.utils.Compression
import timber.log.Timber
import java.io.File
import java.io.FileNotFoundException
import java.util.*

/**
 * (c) Nexus-Computing GmbH Switzerland, 2019
 * Created by Manuel Di Cerbo on 21.03.19.
 */
class ResourcesProvider(val gitlabProjectId: String) {

    data class CommitInformation(val commitId: String, val createdAt: String)

    val client = OkHttpClient()

    fun fetchRepository(directory: File) {
        val request = Request.Builder()
                .url("https://gitlab.com/api/v4/projects/$gitlabProjectId/repository/archive.zip").build()
        val inputStream = client.newCall(request).execute().body()?.byteStream() ?: return
        directory.deleteRecursively()
        Compression().unzip(inputStream, directory, Date().time)
    }

    fun fetchLastCommit(): CommitInformation {
        val request = Request.Builder()
                .url("https://gitlab.com/api/v4/projects/$gitlabProjectId/repository/commits?ref_name=master").build()
        val response = client.newCall(request).execute().body()?.string()

        return CommitInformation(
                JSONArray(response).getJSONObject(0).getString("id"),
                JSONArray(response).getJSONObject(0).getString("created_at")
        )
    }

    @Throws(Exception::class)
    fun allResources(dir: File): List<ExternalResource> {
        val (master, index) = indexAndMaster(dir)
        val arr = JSONArray(index.readText())
        val list = mutableListOf<ExternalResource>()
        for (i in 0 until arr.length()) {
            val item = arr.getJSONObject(i)
            val items = parseItems(File("${master.absolutePath}/${item.getString("file")}"), item.getString("type"))
            list.add(ExternalResource(item.getString("id"), item.optString("description", ""), items))
        }
        return list
    }

    @Throws(Exception::class)
    fun parseResource(directory: File, id: String): ExternalResource {
        val (master, index) = indexAndMaster(directory)

        val json = JSONArray(index.readText())

        for (i in 0 until json.length()) {
            val item = json.getJSONObject(i)
            if (item.getString("id") == id) {
                val items = parseItems(File("${master.absolutePath}/${item.getString("file")}"), item.getString("type"))
                return ExternalResource(id, item.optString("description", ""), items)
            }
        }
        throw Exception("can't find resource: $id")
    }

    private fun indexAndMaster(directory: File): Pair<File, File> {
        val dirs = directory.list()
        val master = dirs.firstOrNull { it.startsWith("resources-master-") }?.let { File(directory, it) }
                ?: throw FileNotFoundException("unable to find masterDirectory")
        val index = File(master, "index.json")
        if (!index.exists()) {
            throw FileNotFoundException("unable to find index file in repository")
        }
        return Pair(master, index)
    }

    fun parseItems(file: File, type: String): List<ResourceItem> {
        if (!file.exists()) {
            throw FileNotFoundException("unable to load file: ${file.absolutePath}")
        }

        val content = file.readText()
        val list = mutableListOf<ResourceItem>()

        return when (type) {
            "json" -> {
                val arr = JSONArray(content)
                for (i in 0 until arr.length()) {
                    val obj = arr.getJSONObject(i)
                    list.add(ResourceItem(
                            obj.getString("id"),
                            obj.getString("name"),
                            obj.optString("description", "")
                    ))
                }
                list
            }
            "csv" -> {
                val lines = content.lines()
                lines.mapNotNull {
                    val args = it.split("\t")
                    Timber.d(args.joinToString(","))
                    if (args.size < 2) {
                        Timber.e("unable to parse line: %s", it)
                        return@mapNotNull null
                    }
                    ResourceItem(args[0], args[1], args.getOrNull(2) ?: "")
                }
            }
            else -> emptyList()
        }
    }

}