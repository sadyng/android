package org.oursci.android.common.farmos

import androidx.room.*

/**
 * Created by Manuel Di Cerbo on 15.03.19.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
@Dao
interface FarmOsDao {

    // server
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertServer(info: List<FarmOsServer>)

    @Query("delete from farmos_server")
    fun deleteAllServers()

    @Delete
    fun deleteServer(server: FarmOsServer)

    @Transaction
    fun deleteServer(id: Long) {
        deleteServer(FarmOsServer(id))
        deleteAreas(id)
    }

    @Query("select * from farmos_server")
    fun getAllServers(): List<FarmOsServer>

    @Query("select * from farmos_server where active = :active")
    fun getActiveServers(active: Boolean = true): List<FarmOsServer>


    // area
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAreas(areas: List<FarmOsArea>)

    @Query("delete from farmos_area")
    fun deleteAllAreas()

    @Delete
    fun deleteArea(area: FarmOsArea)

    @Query("delete from farmos_area where farmOsServer = :serverId")
    fun deleteAreas(serverId: Long)

    @Query("select * from farmos_area")
    fun getAllAreas(): List<FarmOsArea>

    @Query("select * from farmos_area where farmOsServer = :serverId")
    fun getAreas(serverId: Long): List<FarmOsArea>

    @Transaction
    fun getActiveAreas(): List<FarmOsArea> {
        val activeServer = getActiveServers().firstOrNull() ?: return emptyList()
        return getAreas(activeServer.id)
    }


    // planting
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPlantings(plantings: List<FarmOsPlanting>)

    @Query("delete from farmos_planting")
    fun deleteAllPlantings()

    @Delete
    fun deletePlanting(planting: FarmOsPlanting)

    @Query("delete from farmos_planting where farmOsServer = :serverId")
    fun deletePlantings(serverId: Long)

    @Query("select * from farmos_planting")
    fun getAllPlantings(): List<FarmOsPlanting>

    @Query("select * from farmos_planting where farmOsServer = :serverId")
    fun getPlantings(serverId: Long): List<FarmOsPlanting>

    @Transaction
    fun getActivePlantings(): List<FarmOsPlanting> {
        val activeServer = getActiveServers().firstOrNull() ?: return emptyList()
        return getPlantings(activeServer.id)
    }








}