package org.oursci.android.common.ui.survey.measurement

import android.os.Build
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import android.widget.Toast
import kotlinx.android.synthetic.main.control_measurement_result.*
import org.oursci.android.common.*
import org.oursci.android.common.arch.ConsumableObserver
import org.oursci.android.common.databinding.ControlMeasurementResultBinding
import org.oursci.android.common.databinding.LayoutControlFragmentBinding
import org.oursci.android.common.survey.SurveyManager
import org.oursci.android.common.ui.BaseFragment
import org.oursci.android.common.ui.controls.measurement.MeasurementControl
import org.oursci.android.common.ui.controls.measurement.MeasurementDisplay
import timber.log.Timber

/**
 * Created by Manuel Di Cerbo on 09.10.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class MeasurementResultFragment : BaseFragment() {

    private var autoRun = false
    override val searchable = true

    private val measurementViewModel: MeasurementViewModel by lazy {
        ViewModelProviders.of(this, app().viewModelFactory).get(MeasurementViewModel::class.java)
    }
    private lateinit var controlBinding: ControlMeasurementResultBinding
    private lateinit var binding: LayoutControlFragmentBinding

    private var idx: Int = -1
    private var measurementId: String = ""
    private var filePath: String = ""
    private var summary = false

    private val autoRunObserver = ConsumableObserver<String> { t: String?, consumed: Boolean ->
        if (consumed) {
            return@ConsumableObserver
        }


    }

    private val saveRequiredObserver = ConsumableObserver<Void> { t: Void?, consumed: Boolean ->
        if (consumed) {
            return@ConsumableObserver
        }

        if (idx != -1) {
            measurementViewModel.saveResult(idx)
        }
    }

    private val storeMeasurementResultObserver = ConsumableObserver<Boolean> { success, consumed ->
        if (consumed || success == null) return@ConsumableObserver

        if (success) {
            if (idx == -1) {
                app().navigationManager.navigateToMeasurementResultsList()
            } else {
                Toast.makeText(activity, "Survey Result Saved", Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(activity, "error storing result", Toast.LENGTH_SHORT).show()
        }

        if (autoRun && idx >= 0) {
            measurementViewModel.autoRunNext(idx)
        }

    }

    private val resultLoadedObserver = ConsumableObserver<Void> { _, consumed ->
        if (consumed) return@ConsumableObserver
        controlBinding.webviewMeasurementResult.loadUrl("file:///android_asset/processor/processor.html")
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        idx = arguments?.getInt("idx", -1) ?: -1
        measurementId = arguments?.getString("id", "") ?: ""
        filePath = arguments?.getString("file", "") ?: ""
        measurementViewModel.clearResult()
        autoRun = arguments?.getBoolean("autoRun", false) ?: false


        binding = LayoutControlFragmentBinding.inflate(inflater, container, false)

        binding.progress.visibility = View.VISIBLE

        activity.unlockMenus()
        if (!measurementId.isEmpty()) {
            activity.allowOverview(false)
            if (savedInstanceState == null) {
                measurementViewModel.createMeasurementControl(measurementId)
            }
            binding.btRedo.visibility = View.GONE
            binding.btNext.visibility = View.GONE
        } else {
            activity.allowOverview(true)
            if (savedInstanceState == null) {
                measurementViewModel.loadControl(idx, MeasurementControl::class.java)
            }
            binding.btRedo.visibility = View.VISIBLE
        }

        binding.btNext.setOnClickListener {
            if (summary) {
                app().navigationManager.navigateToSurveyResultsList(true, false,
                        SurveyManager.of(app()).currentSurvey?.formId)
                return@setOnClickListener
            }

            if (measurementViewModel.hasMoreQuestions(idx)) {
                app().navigationManager.moveToNextSurveyQuestion(idx + 1)
            } else {
                app().navigationManager.finalOverview()
            }
        }


        binding.viewStubControl.viewStub?.layoutResource = R.layout.control_measurement_result
        binding.viewStubControl.viewStub?.setOnInflateListener { _, view ->
            postInflate(view)
        }

        binding.root.postDelayed({
            binding.viewStubControl.viewStub?.inflate()
        }, 350)

        summary = arguments?.getBoolean("summary", false) ?: false


        return binding.root
    }

    override fun menuItems() = arrayOf(
            BaseActivity.MenuItemInfo(0, R.id.console, 0, "Debug Console", R.drawable.ic_terminal, false, false)
    )

    override fun onMenuItemPressed(menuItemId: Int): Boolean = when (menuItemId) {
        R.id.console -> {
            controlBinding.webviewMeasurementResult.visibility = View.GONE
            controlBinding.textView.visibility = View.VISIBLE
            controlBinding.btSave.visibility = View.GONE
            controlBinding.invalidateAll()

            controlBinding.textView.text = ""
            controlBinding.textView.append(measurementViewModel.consoleLog())
            true
        }
        else -> super.onMenuItemPressed(menuItemId)
    }

    override fun onBackPressed(): Boolean {
        try {
            if (controlBinding.textView.visibility == View.VISIBLE) {
                controlBinding.webviewMeasurementResult.visibility = View.VISIBLE

                if (idx == -1) { // if not simple measurement, save result
                    controlBinding.btSave.visibility = View.GONE
                }

                controlBinding.textView.visibility = View.GONE

                return true
            }
        } catch (e: Exception) {
            // just ignore on back pressed if control is not initialized
            e.printStackTrace()
        }
        return super.onBackPressed()
    }

    private fun postInflate(view: View) {
        controlBinding = ControlMeasurementResultBinding.bind(view)

        if (idx == -1 && filePath.isEmpty()) { // if not simple measurement and not already saved, save result
            controlBinding.btSave.visibility = View.VISIBLE
        }

        if (summary) {
            controlBinding.btSave.visibility = View.GONE
            binding.btNext.visibility = View.VISIBLE
        }

        controlBinding.webviewMeasurementResult.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView?, url: String?) {
                binding.progress.visibility = View.GONE
                // This fixes the accidental second saveResult
//                if (idx != -1) {
//                    measurementViewModel.saveResult(idx)
//                }
                super.onPageFinished(view, url)
            }
        }


        controlBinding.webviewMeasurementResult.webChromeClient = object : WebChromeClient() {

            override fun onPermissionRequest(request: PermissionRequest?) {
                request?.let {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        it.grant(it.resources)
                    }
                }
            }

            override fun onConsoleMessage(consoleMessage: ConsoleMessage?): Boolean {
                if (consoleMessage == null) {
                    return super.onConsoleMessage(consoleMessage)
                }

                val color = when (consoleMessage.messageLevel()) {
                    ConsoleMessage.MessageLevel.DEBUG -> "orange"
                    ConsoleMessage.MessageLevel.ERROR -> "red"
                    ConsoleMessage.MessageLevel.LOG -> "blue"
                    ConsoleMessage.MessageLevel.TIP -> "green"
                    ConsoleMessage.MessageLevel.WARNING -> "orange"
                    else -> "black"
                }

                val text = "<font color='%s'>%s (%d)</font><br><br>".format(color, consoleMessage.message(), consoleMessage.lineNumber())
                measurementViewModel.appendLog(text)
                controlBinding.textView.append(html(text))
                return super.onConsoleMessage(consoleMessage)
            }
        }

        controlBinding.textView.movementMethod = ScrollingMovementMethod()

        controlBinding.webviewMeasurementResult.settings.setAppCacheEnabled(false)
        controlBinding.webviewMeasurementResult.settings.cacheMode = WebSettings.LOAD_NO_CACHE
        controlBinding.webviewMeasurementResult.clearCache(true)


        controlBinding.webviewMeasurementResult.settings.apply {
            cacheMode = WebSettings.LOAD_NO_CACHE
            setAppCacheEnabled(false)
            javaScriptEnabled = true
            allowFileAccessFromFileURLs = true
            allowUniversalAccessFromFileURLs = true
            allowContentAccess = true
            javaScriptCanOpenWindowsAutomatically = true
            mediaPlaybackRequiresUserGesture = false
            domStorageEnabled = true
        }


        controlBinding.webviewMeasurementResult.apply {
            addJavascriptInterface(measurementViewModel.applicationInterface, "android")
            addJavascriptInterface(measurementViewModel.sensorInterface, "sensor")
            addJavascriptInterface(measurementViewModel.processorInterface, "processor")
        }


//        controlBinding.btRedo.visibility = if (filePath.isEmpty()) View.VISIBLE else View.GONE
//        controlBinding.btSave.visibility = if (filePath.isEmpty()) View.VISIBLE else View.GONE


        measurementViewModel.apply {
            resultLoaded.observe(this@MeasurementResultFragment, resultLoadedObserver)
            storeSuccessResult.observe(this@MeasurementResultFragment, storeMeasurementResultObserver)
            saveRequired.observe(this@MeasurementResultFragment, saveRequiredObserver)
            autoRunResult.observe(this@MeasurementResultFragment, autoRunObserver)
        }


        if (filePath.isNotEmpty()) {
            Timber.d("loading file of path: %s", filePath)
            measurementViewModel.loadResultFromPath(filePath)
        } else {
            measurementViewModel.loadAllAnswersAsResult()
            controlBinding.webviewMeasurementResult.loadUrl("file:///android_asset/processor/processor.html")
        }


        binding.btRedo.setOnClickListener {
            measurementViewModel.redoMeasurement(measurementId, idx)
        }

//        if(idx == -1){
//            controlBinding.btSave.visibility = View.VISIBLE
//        }


        controlBinding.btSave.setOnClickListener {
            measurementViewModel.saveResult(idx)
            activity.onMeasurementEnd()
        }
    }
}