package org.oursci.android.common.ui.controls.external

import org.javarosa.form.api.FormEntryPrompt
import org.oursci.android.common.BaseApplication
import org.oursci.android.common.ui.BaseFragment
import org.oursci.android.common.ui.controls.BaseControl
import org.oursci.android.common.ui.controls.IControlAdapter
import org.oursci.android.common.ui.controls.questionId
import org.oursci.android.common.ui.survey.external.ExternalFragment
import org.oursci.android.common.vo.survey.Group
import org.oursci.android.common.vo.survey.text.TextControlAnswer

/**
 * Created by Manuel Di Cerbo on 16.03.19.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
object ExternalControlAdapter : IControlAdapter<ExternalControl> {
    override fun produce(prompt: FormEntryPrompt, group: Group?): ExternalControl? {

        val answerValue = prompt.answerValue
        val answer = answerValue?.value?.toString()
        val hint = prompt.helpText ?: ""
        val info = parseParams(hint)

        return ExternalControl(
                answer = TextControlAnswer(answer = answer),
                defaultValue = prompt.answerText ?: "",
                label = prompt.longText ?: "",
                readOnly = prompt.isReadOnly,
                required = prompt.isRequired,
                requiredText = "this input is required",
                hint = info.hint,
                dataName = prompt.questionId(),
                group = group,
                params = when {
                    info.type == "farmos-area" -> ExternalControl.Params(ExternalControl.Type.FARMOS_AREA, "")
                    info.type == "farmos-planting" -> ExternalControl.Params(ExternalControl.Type.FARMOS_PLANTING, "")
                    info.type == "ontology" -> ExternalControl.Params(ExternalControl.Type.ONTOLOGY, info.name)
                    info.name.isNotEmpty() -> ExternalControl.Params(ExternalControl.Type.GENERIC, info.name, extra = info.type)
                    else -> ExternalControl.Params(ExternalControl.Type.GENERIC, "")
                }
        )
    }

    override fun resolve(app: BaseApplication, control: BaseControl<*>): BaseFragment {
        return ExternalFragment()
    }

    enum class Type {
        TYPE_NAME_HINT,
        TYPE_HINT
    }

    data class Info(
            val hint: String = "",
            val name: String = "",
            val type: String = ""
    )

    val paramMap = arrayOf(
            """^([^;]*);([^;]*);([^;]*)$""".toRegex() to Type.TYPE_NAME_HINT,
            """^([^;]*);([^;]*)$""".toRegex() to Type.TYPE_HINT
    )

    fun parseParams(hintField: String): Info {

        val generic = Info(hintField)
        val matching = paramMap.firstOrNull { (regex) ->
            regex.matches(hintField)
        } ?: return generic

        val r = matching.first.find(hintField) ?: return generic

        return when (matching.second) {
            Type.TYPE_HINT -> Info(r.groupValues[2], type = r.groupValues[1])
            Type.TYPE_NAME_HINT -> Info(r.groupValues[3], r.groupValues[2], r.groupValues[1])
        }
    }

    fun qualifies(helpText: String?): Boolean {
        val hint = helpText ?: return false
        return paramMap.any {
            hint.matches(it.first)
        }
    }

}