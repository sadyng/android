package org.oursci.android.common.ui.qrscanner

import org.oursci.android.common.BaseApplication
import org.oursci.android.common.ui.BaseViewModel

/**
 * Created by Manuel Di Cerbo on 05.11.17.
 * (c) Manuel Di Cerbo, Nexus-Computing GmbH
 */
class QrScannerViewModel(app: BaseApplication) : BaseViewModel(app) {

    var mCurrentQrValue = ""
}