package org.oursci.android.common

import org.joda.time.DateTime
import org.junit.Test
import org.oursci.android.common.resources.ResourcesProvider
import timber.log.Timber
import java.io.File

/**
 * (c) Nexus-Computing GmbH Switzerland, 2019
 * Created by Manuel Di Cerbo on 21.03.19.
 */
class ResourcesTest : BaseTest() {

    val resourcesProvider = ResourcesProvider("11427416")

    @Test
    fun fetchLatestCommit() {
        val sha = resourcesProvider.fetchLastCommit()
        Timber.d(sha.commitId)
    }

    @Test
    fun fetchArchive() {
        val dir = File("../mock/resources")
        resourcesProvider.fetchRepository(dir)
        val externalResource = resourcesProvider.parseResource(dir, "test")
        Timber.d(externalResource.toString())


        val basicOntologies = listOf(
                "tillage",
                "weed_control",
                "pest_desease_control",
                "irrigation",
                "amendments",
                "irrigation_extended"
        )

        basicOntologies.forEach {ontology ->
            resourcesProvider.parseResource(dir, ontology).let {
                Timber.d(it.toString())
            }
        }


        try {
            resourcesProvider.parseResource(dir, "hello")
        } catch (e: Exception) {
            Timber.d("exception caught, OK")
        }
    }

    @Test
    fun parseTimeStamp() {
        val time = DateTime.parse("2019-03-21T11:19:12.000Z")
        Timber.d(time.millis.timeAgo())


        val time2 = DateTime.parse("2150-03-21T11:19:12.000Z")
        Timber.d(time2.millis.timeAgo())
    }
}