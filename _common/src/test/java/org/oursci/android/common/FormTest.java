package org.oursci.android.common;

import org.javarosa.core.model.Constants;
import org.javarosa.core.model.FormDef;
import org.javarosa.core.model.FormIndex;
import org.javarosa.core.model.IFormElement;
import org.javarosa.core.model.QuestionDef;
import org.javarosa.core.model.data.SelectMultiData;
import org.javarosa.core.model.data.StringData;
import org.javarosa.core.model.instance.InstanceInitializationFactory;
import org.javarosa.core.model.instance.TreeElement;
import org.javarosa.core.services.IPropertyManager;
import org.javarosa.core.services.properties.IPropertyRules;
import org.javarosa.core.services.transport.payload.ByteArrayPayload;
import org.javarosa.core.util.externalizable.DeserializationException;
import org.javarosa.form.api.FormEntryCaption;
import org.javarosa.form.api.FormEntryController;
import org.javarosa.form.api.FormEntryModel;
import org.javarosa.form.api.FormEntryPrompt;
import org.javarosa.model.xform.XFormSerializingVisitor;
import org.javarosa.xform.util.XFormUtils;
import org.junit.Before;
import org.junit.Test;
import org.oursci.android.common.javarosa.Event;
import org.oursci.android.common.javarosa.FormLoader;
import org.oursci.android.common.javarosa.JavaRosaInit;
import org.oursci.android.common.ui.survey.mutliplechoice.MultipleChoiceAdapter;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * (c) Nexus-Computing GmbH Switzerland, 2017
 * Created by Manuel Di Cerbo on 23.08.17.
 */

public class FormTest {

    @Before
    public void init() {
        JavaRosaInit.Companion.initializeJavaRosa(new IPropertyManager() {
            @Override
            public List<String> getProperty(String propertyName) {
                return new ArrayList<>();
            }

            @Override
            public void setProperty(String propertyName, String propertyValue) {

            }

            @Override
            public void setProperty(String propertyName, List<String> propertyValue) {

            }

            @Override
            public String getSingularProperty(String propertyName) {
                return "";
            }

            @Override
            public void addRules(IPropertyRules rules) {

            }

            @Override
            public List<IPropertyRules> getRules() {
                return new ArrayList<>();
            }
        });
    }

    public FormEntryController internalForm() throws IOException {
        File xmlFile = new File("./_common/sampledata/reflectance.xml");
        FormLoader loader = new FormLoader();

        File cache = new File("_common/sampledata/cache");
        if (!cache.exists()) {
            if (!cache.mkdirs()) {
                throw new IOException("unable to create cache");
            }
        }


        File shadowDir = new File("_common/sampledata/shadow");
        if (!shadowDir.exists()) {
            if (!shadowDir.mkdirs()) {
                throw new IOException("unable to create shadowdir");
            }
        }

        try (FileInputStream fis = new FileInputStream(xmlFile)) {
            new File("./output_carbon.xml").createNewFile();

            FormEntryController controller = loader.loadForm(fis, new File("./output_carbon.xml"), cache, shadowDir);
            System.out.println("form name " + controller.getModel().getForm().getName());

            return controller;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Test
    public void printMultipleChoice() throws IOException {
        FormEntryController fec = internalForm();
        FormDef def = fec.getModel().getForm();
        FormEntryModel fem = fec.getModel();


        def.initialize(true, new InstanceInitializationFactory());
        String id = fem.getForm().getMainInstance().getRoot().getAttributeValue(null, "id");


        String description = "";
        try {
            description = fem.getForm().getInstance().getRoot()
                    .getChild("meta", 0)
                    .getChild("description", 0).getValue().getDisplayText();
        } catch (Exception e) {
        }

        List<TreeElement> children = fem.getForm().getInstance().getRoot().getChildrenWithName("meta");
        for (TreeElement e : children) {
            List<TreeElement> desc = e.getChildrenWithName("description");

        }


        FormIndex idx = fem.getFormIndex();
        IFormElement element = fec.getModel().getForm().getChild(idx);

        System.out.printf("id of element is %s\n", element.getClass());

        eventLoop:
        for (; ; ) {

            Event e = Event.fromEvent(fem.getEvent(idx));
            System.out.println(e);

            switch (e) {
                case EVENT_BEGINNING_OF_FORM:
                    break;
                case EVENT_END_OF_FORM:
                    break eventLoop;
                case EVENT_PROMPT_NEW_REPEAT:
                    break;
                case EVENT_QUESTION:
                    String drivletId = fem.getQuestionPrompt().getSpecialFormQuestionText("drivlet");
                    System.out.println("class: " + fem.getQuestionPrompt().getAnswerValue().getClass().getCanonicalName());
                    if (fem.getQuestionPrompt().getQuestion().getControlType() == Constants.CONTROL_SELECT_ONE) {
                        System.out.println(fem.getQuestionPrompt().getAnswerValue().getClass().getCanonicalName());
                    }

                    fec.answerQuestion(idx, new StringData("hello"), true);
                    System.out.println("drivlet is: " + drivletId);
                    System.out.println(fem.getQuestionPrompt().getLongText());
                    System.out.println(fem.getQuestionPrompt().getHelpText());
                    System.out.println(fem.getQuestionPrompt().getAnswerText());
                    System.out.println();
                    break;
                case EVENT_GROUP:
                    break;
                case EVENT_REPEAT:
                    break;
                case EVENT_REPEAT_JUNCTURE:
                    break;
                case EVENT_UNKNOWN:
                    break;
            }

            idx = fem.incrementIndex(idx);
            fem.setQuestionIndex(idx);
        }

    }

    @Test
    public void formDef() throws IOException, DeserializationException, InterruptedException {
        System.out.println("reading form definition");


        File xmlFile = new File("./_common/sampledata/form.xml");
        System.out.println("xml file: " + xmlFile.getAbsolutePath());

        FileInputStream fis = new FileInputStream(xmlFile);
        FormDef def = XFormUtils.getFormFromInputStream(fis);

        for (IFormElement e : def.getChildren()) {
            System.out.printf("id, title: %s, %s\n", e.getID(), e.getLabelInnerText());
        }

        FormEntryModel fem = new FormEntryModel(def);
        FormEntryController fec = new FormEntryController(fem);

        def.initialize(true, new InstanceInitializationFactory());
        String id = fem.getForm().getMainInstance().getRoot().getAttributeValue(null, "id");

        FormIndex idx = fem.getFormIndex();
        IFormElement element = fec.getModel().getForm().getChild(idx);

        System.out.printf("id of element is %s\n", element.getClass());

        eventLoop:
        for (; ; ) {

            Event e = Event.fromEvent(fem.getEvent(idx));
            System.out.println(e);

            switch (e) {
                case EVENT_BEGINNING_OF_FORM:
                    break;
                case EVENT_END_OF_FORM:
                    break eventLoop;
                case EVENT_PROMPT_NEW_REPEAT:
                    break;
                case EVENT_QUESTION:
                    String drivletId = fem.getQuestionPrompt().getSpecialFormQuestionText("drivlet");
                    fec.answerQuestion(idx, new StringData("hello"), true);
                    System.out.println("drivlet is: " + drivletId);
                    System.out.println(fem.getQuestionPrompt().getLongText());
                    System.out.println(fem.getQuestionPrompt().getHelpText());
                    System.out.println(fem.getQuestionPrompt().getAnswerText());
                    System.out.println();
                    break;
                case EVENT_GROUP:
                    break;
                case EVENT_REPEAT:
                    break;
                case EVENT_REPEAT_JUNCTURE:
                    break;
                case EVENT_UNKNOWN:
                    break;
            }

            idx = fem.incrementIndex(idx);
            fem.setQuestionIndex(idx);
        }

        XFormSerializingVisitor serializer = new XFormSerializingVisitor();
        ByteArrayPayload payload = (ByteArrayPayload) serializer.createSerializedPayload(fem.getForm().getInstance());
        exportXmlFile(payload, "./output.xml");


        System.out.printf("question event: %d\n", fec.getModel().getEvent());

    }

    static void exportXmlFile(ByteArrayPayload payload, String path) throws IOException {
        File file = new File(path);
        if (file.exists() && !file.delete()) {
            throw new IOException("Cannot overwrite " + path + ". Perhaps the file is locked?");
        }

        // create data stream
        InputStream is = payload.getPayloadStream();
        int len = (int) payload.getLength();

        // read of data stream
        byte[] data = new byte[len];
        // try {
        int read = is.read(data, 0, len);
        if (read > 0) {
            // write xml file
            RandomAccessFile randomAccessFile = null;
            try {
                // String filename = dir + File.separator +
                // dir.substring(dir.lastIndexOf(File.separator) + 1) + ".xml";
                randomAccessFile = new RandomAccessFile(file, "rws");
                randomAccessFile.write(data);
            } finally {
                if (randomAccessFile != null) {
                    try {
                        randomAccessFile.close();
                    } catch (IOException e) {
                        System.err.printf("Error closing RandomAccessFile: %s\n", path);
                    }
                }
            }
        }
    }


}
